<?php 
/* 
Template Name: Locations Single
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	<?php
		$location = get_field('location');
		$location = preg_replace('/\s*/', '', $location);
		$location = strtolower($location);
	?>
	<div id="<?php echo $location.'location' ?>" class="location_wrapper">
		<div class="container section">
			<div class="intro">
				<h2><?php the_title(); ?></h2>
				<hr>
				<?php if( get_field('phone') || get_field('fax') || get_field('address') ): ?>
				<p><strong>Phone:</strong> <?php the_field('phone'); ?> <strong>Fax:</strong> <?php the_field('fax'); ?>
				<p><strong>Address:</strong> <?php the_field('address'); ?></p>
				<?php endif; ?>
			</div>
		</div>
		
		<?php 
			$map = get_field('google_map');
			if( !empty($map) ):
		?>
		<div class="map_wrapper">
			<div class="location_map">
				<div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>"></div>
			</div>
			<div class="location_image">
				<?php 
					$thumb_id = get_post_thumbnail_id();
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
					$thumb_url = $thumb_url_array[0];
				?>
				<img src="<?php echo $thumb_url; ?>" />
			</div>
		</div>
		<?php endif; ?>
		
		<?php 
			$city = get_field('location'); 
			$city = preg_replace('/\s*/', '', $city);
			$city = strtolower($city);
		?>
		<?php if(have_rows('team_members')) : ?>
		<div class="section container">
			<h2 class="line">The Team</h2>
			<div class="team_wrapper" id="<?php echo $city; ?>">
			<ul class="tab_nav team_headshots">
			<?php $count = 1; ?>
			<?php while(have_rows('team_members')) : the_row(); ?>
				<li>	
					<a href="#" name="<?php echo '#'.$city.$count++ ?>">
					<?php if(get_sub_field('headshot')) : ?>
						<?php 
							$image = get_sub_field('headshot');
							$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)]
							echo wp_get_attachment_image( $image, $size ); 
						?>
					<?php else : ?>
						<img src="<?php echo get_template_directory_uri(); ?>/images/team_placeholder.jpg" alt="<?php the_field('name'); ?>" />
					<?php endif; ?>
					</a>
				</li>
			<?php endwhile; ?>
			</ul>
			<!-- end .team_headshots -->
			
			<div class="tab_wrapper bio_wrapper">
			<?php $count = 1; ?>
			<?php while(have_rows('team_members')) : the_row(); ?>
				<div id="<?php echo '#'.$city.$count++ ?>">
					<p class="bio_meta"><?php the_sub_field('name'); ?> <?php if(get_sub_field('title')) : ?>- <span><?php the_sub_field('title'); ?></span><?php endif; ?></p>
					<?php the_sub_field('bio'); ?>
				</div>
			<?php endwhile; ?>
			</div><!-- end .bio_wrapper -->
			</div>
		</div>
		<?php endif; ?> 
		
		<?php if(have_rows('services')) : ?>
		<div class="services_wrapper small_section">
			<div class="container">
				<div class="section_header">
					<h2 class="line">Location Specific Services</h2>
					<p class="button"><a href="<?php echo get_permalink(17); ?>" class="button-orange-solid">Contact Us</a></p>
				</div>
				<ul class="services_list">
				<?php while(have_rows('services')) : the_row(); ?>
					<li>
						<div class="services_left">
							<h4><?php the_sub_field('title'); ?></h4>
							<?php if(get_sub_field('hours_&_rates')): ?><div class="hours_rates"<?php the_sub_field('hours_&_rates'); ?></div><?php endif; ?>
						</div>
						<div class="services_right">
							<?php the_sub_field('description'); ?>
						</div>
					</li>
				<?php endwhile; ?>
				</ul>
			</div>
		</div>
		<?php endif; ?>
		
		<div class="jobs container small_section nopadbottom">
			<h2 class="line">Open Job Positions</h2>
			<script src='https://www.workable.com/assets/embed.js' type='text/javascript'></script>
			<script type='text/javascript' charset='utf-8'>
			whr(document).ready(function(){
			whr_embed(12941, {detail: 'descriptions', base: 'jobs', zoom: 'city', grouping: 'none'});
			});
			</script>
			<div id="whr_embed_hook"></div>
		</div>
		
	</div>
	<?php if(have_rows('best_place_to_work', 11)) : ?>
		<div class="container aligncenter">
		<ul class="btw clearfix">
		<?php while(have_rows('best_place_to_work', 11)) : the_row(); ?>
			<li>
				<?php 
					$image = get_sub_field('image');
					$size = 'team-thumb'; // (thumbnail, medium, large, full or custom size)]
					echo wp_get_attachment_image( $image, $size ); 
				?>
			</li>
		<?php endwhile; ?>
		</ul>
		</div>
	<?php endif; ?>
	
	
<?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>
