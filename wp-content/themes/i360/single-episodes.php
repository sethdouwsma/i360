<?php get_header(); ?>

	<div class="content section container small_container">
	  	<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'podcast'); ?>
			<?php endwhile; ?>
	  	<?php else : ?>
	  		<h1>There are currently no posts</h1>
	  	<?php endif; ?>
	  	<?php wp_reset_query(); ?>
	
		<div class="comments">
			<?php comments_template(); ?> 
		</div>
	</div><!-- end content -->

<?php get_footer(); ?>