<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 */
?>
			
	</div><!-- end #main -->		
				
	<footer>
		<div class="container">
			<div class="upper_footer">
				<ul class="footer_section">
					<h4>Contact</h4>
					<li>Tel: <?php the_field('phone', 5); ?></li>
					<li>Fax: <?php the_field('fax', 5); ?></li>
					<li>Email: <a href="mailto:<?php the_field('email', 5); ?>"><?php the_field('email', 5); ?></a></li>
					<li><?php the_field('address', 5); ?></li>
					<li><?php the_field('city', 5); ?>, <?php the_field('state', 5); ?> <?php the_field('zip_code', 5); ?></li>
					<li><a href="https://crm.bestnotes.com/portal/innovation" target="_blank">Intake Portal</a></li>
				</ul>
				<ul class="footer_section">
					<h4>Locations</h4>
					<?php
						$children = get_pages( 
					    array(
					        'sort_column' => 'menu_order',
					        'sort_order' => 'ASC',
					        'hierarchical' => 0,
					        'parent' => 11,
					    ));
					 ?>
					 <?php foreach( $children as $post ) : setup_postdata( $post ); ?>
					    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php endforeach; ?>
					<li><a href="<?php echo get_permalink(4875); ?>">Careers</a></li>
				</ul>
				<ul class="footer_section">
					<h4>Programs</h4>
					<?php if(have_rows('footer_programs_nav', 5)) : ?>
						<?php while(have_rows('footer_programs_nav', 5)): the_row(); ?>
							<li><a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('name'); ?></a></li>
						<?php endwhile; ?>
					<?php endif; ?>
				</ul>
				<ul class="footer_section subscribe">
					<h4>Stay In Touch</h4>
					<p><?php the_field('subscribe', 5); ?></p>
					<p><a href="http://visitor.r20.constantcontact.com/d.jsp?llr=u77av5iab&amp;p=oi&amp;m=1109130245012&amp;sit=dwue6kugb&amp;f=dff45ff2-d2f2-49fc-a4fc-b76b84f2190b" class="button-orange-solid">Subscribe</a></p> 
					<ul class="social_links">
						<li><a href="<?php the_field('vimeo', 5); ?>" target="_blank"><img src="<?php echo get_template_directory_uri();?>/images/vimeo_white.svg" alt="Vimeo"></a></li>
						<li><a href="<?php the_field('twitter', 5); ?>" target="_blank"><img src="<?php echo get_template_directory_uri();?>/images/twitter_white.svg" alt="Twitter"></a></li>
						<li><a href="<?php the_field('facebook', 5); ?>" target="_blank"><img src="<?php echo get_template_directory_uri();?>/images/facebook_white.svg" alt="Facebook"></a></li>
					</ul>           
				</ul>
			</div>
			<div class="footer_divider">
				<div class="divider_logo_wrapper">
					<span class="padding"></span>
					<p class="divider_logo"><img src="<?php echo get_template_directory_uri(); ?>/images/nav_logo.svg" /></p>
				</div>
			</div>
			<div class="lower_footer">
				<p class="copyright">&copy; <?php echo date("Y") ?> Innovation360®. All Rights Reserved.</p>
				<p class="credentials"><a href="http://edencreative.co" target="_blank">Site Crafted By Eden</a></p>
			</div>
		</div>
	</footer>

	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/tabs.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.glide.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.singlePageNav.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>
	
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>        
    <script type="text/javascript">
	(function($) {
	
	/*
	*  render_map
	*
	*  This function will render a Google Map onto the selected jQuery element
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	$el (jQuery element)
	*  @return	n/a
	*/
	
	function render_map( $el ) {
	
		// var
		var $markers = $el.find('.marker');
	
		// vars
		var args = {
			zoom		: 16,
			scrollwheel	: false,
			center		: new google.maps.LatLng(0, 0),
			mapTypeId	: google.maps.MapTypeId.ROADMAP
		};
	
		// create map	        	
		var map = new google.maps.Map( $el[0], args);
	
		// add a markers reference
		map.markers = [];
	
		// add markers
		$markers.each(function(){
	
	    	add_marker( $(this), map );
	
		});
	
		// center map
		center_map( map );
	
	}
	
	/*
	*  add_marker
	*
	*  This function will add a marker to the selected Google Map
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	$marker (jQuery element)
	*  @param	map (Google Map object)
	*  @return	n/a
	*/
	
	function add_marker( $marker, map ) {
	
		// var
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
	
		// create marker
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: map
		});
	
		// add to array
		map.markers.push( marker );
	
		// if marker contains HTML, add it to an infoWindow
		if( $marker.html() )
		{
			// create info window
			var infowindow = new google.maps.InfoWindow({
				content		: $marker.html()
			});
	
			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {
	
				infowindow.open( map, marker );
	
			});
		}
	
	}
	
	/*
	*  center_map
	*
	*  This function will center the map, showing all markers attached to this map
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	map (Google Map object)
	*  @return	n/a
	*/
	
	function center_map( map ) {
	
		// vars
		var bounds = new google.maps.LatLngBounds();
	
		// loop through all markers and create bounds
		$.each( map.markers, function( i, marker ){
	
			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
	
			bounds.extend( latlng );
	
		});
	
		// only 1 marker?
		if( map.markers.length == 1 )
		{
			// set center of map
		    map.setCenter( bounds.getCenter() );
		    map.setZoom( 16 );
		}
		else
		{
			// fit to bounds
			map.fitBounds( bounds );
		}
	
	}
	
	/*
	*  document ready
	*
	*  This function will render each map when the document is ready (page has loaded)
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/
	
	$(document).ready(function(){
	
		$('.location_map').each(function(){
	
			render_map( $(this) );
	
		});
	
	});
	
	})(jQuery);
	</script>
	
	<?php wp_footer(); ?>
	
</body>
</html>
