<?php 
/* 
Template Name: Videos
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>

	<div class="section container">
		<h2 class="line">Videos</h2>
	  	<?php if (have_rows('videos')) : ?>
		  	<ul class="videos_wrapper">
				<?php while (have_rows('videos')) : the_row(); ?>
					<li class="video_bucket">
						<div class="video"id="<?php the_sub_field('video_id'); ?>" data-reveal-id="videoModal">
							<?php
								$imgid = get_sub_field('video_id');
								$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$imgid.php"));
								$imagesrc = $hash[0]['thumbnail_large'];
							?>
							<p class="watch_video">Watch Video</p>
							<div class="overlay"></div> 
							<p><img src="<?php echo $imagesrc ?>"></p> 
						</div>
						<div class="entry_content">
							<h2 class="entry_title" style="text-transform: none;"><?php the_sub_field('title'); ?></h2>
							<?php the_sub_field('description'); ?>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
	
			<div class="modal">
				<div class="vid_wrap">
					<div class="embed_container">
						
					</div>
				</div>
				<div class="close_link"><img src="<?php echo get_template_directory_uri(); ?>/images/close.svg" /></div>
			</div>
	  	<?php endif; ?>
		<?php wp_reset_query(); ?>
	
	</div>
	
<?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>
