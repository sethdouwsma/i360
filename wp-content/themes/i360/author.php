<?php 
// Main Archive Template
?>

<?php get_header(); ?>

	<header>
		<div class="container">
			<h1>Our <span>Two</span> Cents</h1>
			<h3>Articles by <?php the_author(); ?></h3>
		</div>	
	</header><!-- end header -->

	<div class="content section container">
		<ul class="journal_wrap stacked">
			<?php
				$curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
			 	$args = array(
		            'posts_per_page' => -1,
		            'author'	=> $curauth->ID,
				    'orderby'	=> 'date',
					'order'		=> 'DESC' //  Newst To Oldest
		        );
				query_posts( $args ); 
			?>
		  	<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'index_excerpt' ); ?>
				<?php endwhile; ?>
		  	<?php endif; ?>
			<?php wp_reset_query(); ?>
		</ul>		
	</div><!-- end content -->

<?php get_footer(); ?>