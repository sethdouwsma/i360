<?php
/**
 * Template Name: Custom RSS Template - Podcast
 */

query_posts('post_type=episodes&posts_per_page=-1');

header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
?><rss version="2.0"
        xmlns:content="http://purl.org/rss/1.0/modules/content/"
        xmlns:wfw="http://wellformedweb.org/CommentAPI/"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:atom="http://www.w3.org/2005/Atom"
        xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
        xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
        xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
        <?php do_action('rss2_ns'); ?>>
<channel>
		<atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
		<title>i360 Life Radio</title>
        <link><?php bloginfo_rss('url') ?></link>
		<copyright>&#xA9; Innovation 360</copyright>
		<itunes:author>Innovation 360</itunes:author>
		<description>i360 Life Radio provides hope for people struggling to move forward in life. You or a loved one may be battling mental health issues, wrestling with substance abuse, or facing emerging adulthood, but we believe that you can have a very different life. Join i360 as we discuss what it means to struggle well, to find meaning in the midst of pain and suffering, to find a better way to live.</description>
		<itunes:owner>
		    <itunes:name>Innovation 360</itunes:name>
		    <itunes:email>corporate@i360life.com</itunes:email>
		</itunes:owner>
		<itunes:image href="<?php echo get_template_directory_uri(); ?>/images/podcast.jpg" />
		<language>en</language>
		<itunes:explicit>no</itunes:explicit>
		<itunes:category text="Health" />
		<itunes:keywords>innovation 360, life development, mental health, emerging adulthood, addiction, chemical dependency, family therapy, health, fitness, depression, anxiety, parenting, dallas, fort worth, texas</itunes:keywords>
		
        <?php while(have_posts()) : the_post(); ?>
        		<?php 
	        		$attachment_id = get_field('audio_file');
					$url = wp_get_attachment_url( $attachment_id );
					$title = get_the_title( $attachment_id );
					
					// part where to get the filesize
					$filesize = filesize( get_attached_file( $attachment_id ) );
					$filesize = size_format($filesize, 2);
				?>
                <item>
					<title><?php the_title_rss(); ?></title>
					<itunes:author>Innovation 360</itunes:author>
					<itunes:subtitle><?php the_excerpt_rss(); ?></itunes:subtitle>
					<description><?php the_excerpt_rss(); ?></description>
		            <enclosure url="<?php echo $url; ?>" length="<?php echo $filesize; ?>" type="audio/mpeg"/>
					<guid isPermaLink="false"><?php the_guid(); ?></guid>
					<pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
                </item>
        <?php endwhile; ?>
</channel>
</rss>