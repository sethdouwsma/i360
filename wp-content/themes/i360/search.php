<?php 
// Floor Plan Search
?>

<?php get_header(); ?>

	<header class="lwood">
		<div class="container">
		<?php if( get_field('hero_header', 7) ): ?>
			<h1><?php the_field('hero_header', 7); ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
		<?php the_field('hero_paragraph', 7); ?>
		</div>
	</header><!-- end header -->
	
	<div id="main">

	<div class="content section container">
		<?php
			$args = array( 'post_type' => 'floor_plan' );
			$args = array_merge( $args, $wp_query->query );
			query_posts( $args );
		?>
		
		<?php if ( have_posts() ) : ?>
			<h3 class="search_header">Search Results for "<?php the_search_query(); ?>":</h3>
		<?php else : ?>
			<h3 class="search_header">No Floor Plans Found for "<?php the_search_query(); ?>" </h3>
		<?php endif; ?>
		
		<ul class="floorplan_buckets">
		    <?php while ( have_posts() ) : the_post();
		      // This "The Loop" where search results are output
		      get_template_part( 'content', 'floorplan' );
		    endwhile; ?>
	   </ul><!-- end floorplans -->
	   
		<?php wp_reset_query(); ?>
	
		<?php get_sidebar('floor_plan'); ?>
	</div><!-- end content -->

<?php get_footer(); ?>