<?php 
/* 
Template Name: Locations
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	
	<div class="container section">
		<div class="intro">
			<h2>Locations</h2>
			<hr>
			<?php the_field('locations_intro'); ?>
		</div>
		<div class="location_bucket_wrapper">
		<?php
			$children = get_pages( 
		    array(
		        'sort_column' => 'menu_order',
		        'sort_order' => 'ASC',
		        'hierarchical' => 0,
		        'parent' => 11,
		    ));
		 ?>
		 <?php foreach( $children as $post ) : setup_postdata( $post ); ?>
		 	<?php 
				$thumb_id = get_post_thumbnail_id();
				$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
				$thumb_url = $thumb_url_array[0];
			?>
		    <div class="section location_bucket" style="background-image: url(<?php echo $thumb_url;?>);">
			    <a href="<?php the_permalink(); ?>" class="block_link"><?php the_title(); ?></a>
		    	<div class="pattern"></div>
		    	<div class="container aligncenter">
			        <h1><?php the_title(); ?></h1>
			        <p class="button"><a href="<?php the_permalink(); ?>" class="button-orange-solid">View More Information</a></p>
		    	</div>
		    </div>
		<?php endforeach; ?>
		 <div class="section location_bucket" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/more_locations.jpg);">
			    <a href="mailto:corporate@i360life.com" class="block_link">Coming Soon...</a>
		    	<div class="pattern"></div>
		    	<div class="container aligncenter">
			        <h1>More Locations Coming Soon...</h1>
			        <p>Have a suggestion on bringing i360 to your city? Feel free to email us your suggestions.</p>
			        <p class="button"><a href="mailto:corporate@i360life.com" class="button-orange-solid">Suggest A Location</a></p>
		    	</div>
		    </div>
		</div>
	</div>
	
<?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>
