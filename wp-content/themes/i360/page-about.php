<?php 
/* 
Template Name: About
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	
	<div class="container section">
		<div class="intro">
			<h2>Our Story</h2>
			<hr>
			<?php the_field('our_story'); ?>
			<div class="our_story_video">
				<div class="embed_container"><?php the_field('our_story_video'); ?></div>
			</div>
		</div>
	</div>
	
	<div class="philosophy_wrapper section" data-stellar-ratio="2">
		<div class="pattern"></div>
		<div class="container">
			<div class="philosophy_left">
				<h2 class="line">Treatment Approach / Philosophy</h2>
				<?php the_field('approach_philosophy'); ?>
			</div>
			<div class="philosophy_right">
				<?php the_field('philosophy_quote'); ?>
			</div>
		</div>
	</div>
	
	<div class="press_releases container section">
		<div class="section_header">
			<h2 class="line">Press Releases</h2>
			<p class="button"><a href="<?php echo get_permalink(13);?>" class="button-gray-solid">All Articles</a></p>
		</div>
		<?php
		 	$args = array(
	            'posts_per_page' => 6,
	            'cat'		=> 2,
			    'orderby'	=> 'date',
				'order'		=> 'DESC' //  Newst To Oldest
	        );
			query_posts( $args ); 
		?>
	  	<?php if ( have_posts() ) : ?>
	  		<div class="press_releases_wrapper ">
			  	<ul class="tab_nav press_releases_nav">
				  	<?php $count = 1; ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<li>
							<p class="press_release_date"><?php the_time('F j, Y'); ?></p>
							<p><a href="#" name="<?php echo '#'.$count++ ?>"><?php the_title(); ?></a></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<div class="tab_wrapper press_releases_content">
				  	<?php $count = 1; ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<div id="<?php echo '#'.$count++ ?>" class="press_release">
							<?php the_content(); ?>
						</div>
					<?php endwhile; ?>
				</div>
	  		</div>
	  	<?php endif; ?>
		<?php wp_reset_query(); ?>
	</div>	
	
	<div class="additional_information container intro section">
		<h2>Additional Information</h2>
		<hr>
		<?php the_field('additional_information'); ?>
		<?php if(have_rows('forms')): ?>
			<ul class="forms">
			<?php while(have_rows('forms')) : the_row(); ?>
				<?php if(get_sub_field('form')) : ?>
					<li><a href="<?php the_sub_field('form'); ?>" target="_blank"><?php the_sub_field('name'); ?></a></li>
				<?php elseif(get_sub_field('link')) : ?>
					<li><a href="<?php the_sub_field('link'); ?>" target="_blank"><?php the_sub_field('name'); ?></a></li>
				<?php endif; ?>
			<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	</div>
	
<?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>
