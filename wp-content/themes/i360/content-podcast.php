<?php
/**
 * The default template for displaying post content in podcast content
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry_thumb">
		<?php if(get_the_post_thumbnail()) : ?>
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
		<?php else : ?>
			<a href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/podcast.jpg" alt="Innovation 360 Podcast"></a>
		<?php endif; ?>
	</div>
	<div class="entry_content">
		<div class="entry_date"><span><?php the_time('F j, Y'); ?></span></div>
		<h2 class="entry_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<p style="margin-bottom: 0;"><?php the_terms( $post->ID, 'podcast-categories', 'Posted in ', ', ', ' ' ); ?></p>
		<p class="audio_links"><?php if(get_field('episode_length')) : ?><span><em>Length: <?php the_field('episode_length'); ?></em><?php endif; ?></span> <span>Subscribe on <a href="https://itunes.apple.com/us/podcast/i360-life-radio/id988933660?mt=2" target="_blank">iTunes</a> or <a href="<?php echo get_site_url(); ?>/feed/podcast">RSS</a></span> <span><a href="<?php echo $url; ?>">Download MP3</a></span></p>
		<div class="entry_audio">
			<?php 
	    		$attachment_id = get_field('sermon_audio');
				$url = wp_get_attachment_url( $attachment_id );
				$title = get_the_title( $attachment_id );
				$attr = array(
					'src'      => $url,
					'loop'     => '',
					'autoplay' => '',
					'preload' => 'none'
				);
				echo wp_audio_shortcode( $attr );
			?>
		</div>
		<?php the_content(); ?>
	</div>
</article>