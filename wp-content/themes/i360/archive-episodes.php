<?php 
// Main Archive Template
?>

<?php get_header(); ?>

	<div class="categories_wrapper small_container aligncenter">
		<h2>Podcast Categories</h2>
		<hr>
			<?php 
				$terms = get_terms( 'podcast-categories' );
				if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
				     echo '<ul class="categories clearfix">';
				     foreach ( $terms as $term ) {
				       echo '<li>'.'<a href="' . get_term_link( $term ) . '">' . $term->name . '</a></li>';
				        
				     }
				     echo '</ul>';
				 }
			?>
	</div>

	<div class="content section container small_container">
	  	<?php if ( have_posts() ) : ?>
		  	<ul class="news_buckets vertical">
				<?php while ( have_posts() ) : the_post(); ?>
					<li class="news_bucket">
						<?php get_template_part( 'content', 'podcast-bucket' ); ?>
					</li>
				<?php endwhile; ?>
			</ul>
	  	<?php else : ?>
	  		<h1>There are currently no posts</h1>
	  	<?php endif; ?>
	  	<?php wp_reset_query(); ?>
	  	
	  	<div class="pagination">
	  		<?php wpex_pagination(); ?>
	  	</div>
	</div><!-- end content -->

<?php get_footer(); ?>