<!DOCTYPE HTML>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>

	<meta charset="utf-8">
	
	<!-- Google Chrome Frame for IE -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title><?php wp_title('|',true,'right'); ?></title>
	
	<!-- mobile meta (hooray!) -->
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no"/>
	
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
	<!--[if IE]>
		<link rel="shortcut icon" href="/favicon.ico">
	<![endif]-->
	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css" type="text/css">
	
	<!--[if IE]><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/ie.css" type="text/css" media="screen"><![endif]-->
	<!--[if IE]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	
	<script src="//use.typekit.net/uhv8dol.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
	
	<?php wp_head(); ?>
	
	<!-- drop Google Analytics Here -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-25803438-1', 'auto');
	  ga('send', 'pageview');
	
	</script>
	<!-- end analytics -->

</head>

<body <?php body_class(); ?>>

	<?php 
		$thumb_id = get_post_thumbnail_id();
		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
		$thumb_url = $thumb_url_array[0];
		
		$page_id = "13";
		$image = wp_get_attachment_image_src( get_post_thumbnail_id($page_id), 'single-post-thumbnail' );
		$image_URI = $image[0];
		
		$page_id2 = "11";
		$image2 = wp_get_attachment_image_src( get_post_thumbnail_id($page_id2), 'single-post-thumbnail' );
		$image_URI2 = $image2[0];
	?>
	
	<?php if ( has_post_thumbnail() && !is_tree(11) && !is_home() && !is_single() && !is_category() ) : ?>
	<header style="background-image: url(<?php echo $thumb_url; ?>);" data-stellar-ratio="2">
	<?php elseif( is_tree(11) ) : ?>
	<header style="background-image: url(<?php echo $image_URI2; ?>);" data-stellar-ratio="2">
	<?php elseif( is_home() || is_single() || is_category() ) : ?>
	<header style="background-image: url(<?php echo $image_URI; ?>);" data-stellar-ratio="2">
	<?php elseif( is_archive('episodes') || is_singular('episodes')) : ?>
	<header style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/podcast_header.jpg);" data-stellar-ratio="2">
	<?php else : ?>
	<header style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/home_header.jpg);" data-stellar-ratio="2">
	<?php endif; ?>
	
		<div class="pattern"></div>
		<div class="logo"><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/header_logo.svg" alt="Innovation 360" /></a></div>
		<?php if(is_page('Home')) : ?>
		<div class="container">
			<h1><?php the_field('page_header'); ?></h1>
			<h3><?php the_field('header_content'); ?></h3>
		</div>	
		<?php elseif(is_tree(11)) : ?>
		<div class="container">
			<h1><?php the_field('page_header', 11); ?></h1>
			<p><?php the_field('header_content', 11); ?></p>
		</div>
		<?php elseif(is_home() || is_single() || is_category()) : ?>
		<div class="container">
			<h1><?php the_field('page_header', 13); ?></h1>
			<p><?php the_field('header_content', 13); ?></p>
		</div>
		<?php elseif( is_archive('episodes') || is_singular('episodes')) : ?>
		<div class="container">
			<h1>i360 Podcast</h1>
			<p>We created i360 Life Radio to provide insights into life’s challenges.<br>We hope you enjoy.</p>
		</div>
		<?php else : ?>
		<div class="container">
			<h1><?php the_field('page_header'); ?></h1>
			<p><?php the_field('header_content'); ?></p>
		</div>	
		<?php endif; ?>
		<a href="#" class="scroll_down"><img src="<?php echo get_template_directory_uri(); ?>/images/down_arrow.svg" /></a>
	</header><!-- end header -->
	
	<div id="main">

		<div class="main_nav not_sticky">
			<div class="container clearfix">
				<div class="logo"><a href="<?php echo get_permalink(5); ?>">Innovation 360</a></div>
				<a class="header_toggle"><span></span></a>
				<?php
					$defaults = array(
						'theme_location'  => '',
						'menu'            => '',
						'container'       => '',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => '',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="main_nav" class="navigation">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					);
					
					wp_nav_menu( $defaults );
				?>
			</div>
		</div>