<?php 
/* 
Template Name: Careers
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>

	<div class="jobs container section">
		<h2 class="line">Open Job Positions</h2>
		<script src='https://www.workable.com/assets/embed.js' type='text/javascript'></script>
		<script type='text/javascript' charset='utf-8'>
		whr(document).ready(function(){
		whr_embed(12941, {detail: 'descriptions', base: 'jobs', zoom: 'city', grouping: 'none'});
		});
		</script>
		<div id="whr_embed_hook"></div>
	</div>
		
<?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>
