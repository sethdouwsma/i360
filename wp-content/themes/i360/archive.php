<?php 
// Main Archive Template
?>

<?php get_header(); ?>

	<div class="categories_wrapper small_container aligncenter">
		<h2>Categories</h2>
		<hr>
		<ul class="categories clearfix">
			<?php wp_list_categories('title_li=&orderby=name'); ?>
		</ul>
	</div>

	<div class="content section container small_container">
	  	<?php if ( have_posts() ) : ?>
		  	<ul class="news_buckets vertical">
				<?php while ( have_posts() ) : the_post(); ?>
					<li class="news_bucket">
						<?php get_template_part( 'content', 'excerpt' ); ?>
					</li>
				<?php endwhile; ?>
			</ul>
	  	<?php else : ?>
	  		<h1>There are currently no posts</h1>
	  	<?php endif; ?>
	  	<?php wp_reset_query(); ?>
	  	
	  	<div class="pagination">
	  		<?php wpex_pagination(); ?>
	  	</div>
	</div><!-- end content -->

<?php get_footer(); ?>