<?php
/**
 * The default template for displaying post content in horizontal buckets
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry_thumb">
		<div class="entry_date"><span><?php the_time('m/d'); ?></span></div>
		<?php if(get_the_post_thumbnail()) : ?>
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('horziontal-bucket'); ?></a>
		<?php else : ?>
			<a href="<?php the_permalink(); ?>"><?php echo wp_get_attachment_image( 21, 'horziontal-bucket' ); ?></a>
		<?php endif; ?>
	</div>
	<div class="entry_content">
		<p class="entry_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>		
		<?php
		  $excerpt = get_the_excerpt();
		  echo string_limit_words($excerpt,20);
		?>
		<p class="read_more"><a href="<?php the_permalink(); ?>">Read Full Article</a></p>
	</div>
</article>