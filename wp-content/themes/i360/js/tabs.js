function resetTabs(){
    $(".tab_wrapper > div").hide(); // Hide all content
    $(".tab_nav a").attr("id",""); // Reset id's      
}

var myUrl = window.location.href; //get URL
var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For mywebsite.com/tabs.html#tab2, myUrlTab = #tab2     
var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

(function(){
    $(".tab_wrapper > div").hide(); // Initially hide all content
    $(".tab_nav").find("li:first a").attr("id","current"); // Activate first tab in each section
    $(".tab_wrapper").find("div:first").show(); // Hide all content but first one in each section.
    
    $(".tab_nav a").hover(function(e) {
    	if (!$('body').hasClass("page-id-9")){
	    	return false;
    	}
    	else {
	        e.preventDefault();
	        var selected = $(this).attr("name");
	        if ($(this).attr("id") == "current"){ //detection for current tab
	        	return       
	        }
	        else{             
		        resetTabs();
		        $(this).attr("id","current"); // Activate this
		        $('.tab_wrapper div[id="' + selected + '"]').fadeIn(); // Show content for current tab
	        }
    	}
    });
    
    $(".tab_nav a").on("click",function(e) {
    	if ($('body').hasClass("page-id-11")){
	    	e.preventDefault();
	        var selected = $(this).attr("name");
	        var parent = $(this).closest(".team_wrapper");
	        if ($(this).attr("id") == "current"){ //detection for current tab
	        	return       
	        }
	        else {
			    parent.find(".tab_wrapper div").each(function() {
			    	$(this).hide();
			    });
			    parent.find(".tab_nav a").each(function() {
			    	$(this).attr("id","");
			    });
		        $(this).attr("id","current"); // Activate this
		        $('div[id="' + selected + '"]').fadeIn(); // Show content for current tab
	        }
    	}
    	else {
	        e.preventDefault();
	        var selected = $(this).attr("name");
	        if ($(this).attr("id") == "current"){ //detection for current tab
	        	return       
	        }
	        else{             
		        resetTabs();
		        $(this).attr("id","current"); // Activate this
		        $('.tab_wrapper div[id="' + selected + '"]').fadeIn(); // Show content for current tab
	        }
    	}
    });

    for (i = 1; i <= $(".tab_nav li").length; i++) {
      if (myUrlTab == myUrlTabName + i) {
          resetTabs();
          $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
          $(myUrlTab).fadeIn(); // Show url tab content        
      }
    }
})()