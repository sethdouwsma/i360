jQuery(document).ready(function ($) {

	$(function() {
		$( 'header' ).css( 'height', $( window ).height());
	});
	
	$( window ).resize(function() {
		$( 'header' ).css( 'height', $( window ).height());
	});

	var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
	
	if( iOS ){
		$('.testimonials').css("background-attachment", "scroll");
		$('.philosophy_wrapper').css("background-attachment", "scroll");
		$('header').css("background-attachment", "scroll");
	}

	// Header toggle
	$('.header_toggle').click(function() {
		$('body').toggleClass( "toggled" );
	});

	// Single Page Nav
	$('body.page-id-9 .secondary_nav .navigation').singlePageNav({
	    offset: $('.secondary_nav').height()
	});
	
	$('.locations_nav').singlePageNav();
	
	$( '.scroll_down' ).click(function(e) {
		e.preventDefault();
		$('html, body').animate({
		    scrollTop: ($('#main').first().offset().top)
		},400);
	});
	
	// Sliders
    $('.testimonial_slider').glide({
        autoplay: false,
        arrows: '.testimonials',
        navigation: false
    });
	
	// Sticky Nav
	$(window).scroll(function(){
	     var top 	 = $(this).scrollTop();
		 var height	 = $('header').height();
	     var height2 = $('.main_nav').outerHeight(true);
	     var height3 = $('.secondary_nav').outerHeight(true);
	     	 
	     if(top >= height && !$('body').hasClass('page-id-9') && !$('body').hasClass('.single')) {
	         $('.main_nav').removeClass('not_sticky').addClass('sticky'); 
	         $('#main').css("padding-top", height2);   
	     }
	     else if (top >= (height+height2) && $('body').hasClass('page-id-9')) {
	         $('.secondary_nav').removeClass('not_sticky').addClass('sticky'); 
	         $('#main').css("padding-top", height3);
	     }
	     
	     if(top < height && $('.main_nav').hasClass('sticky')) {
	         $('.main_nav').removeClass('sticky').addClass('not_sticky');  
	         $('#main').css("padding-top", 0);     
	     }
	     else if(top < (height+height2) && $('.secondary_nav').hasClass('sticky')) {
	         $('.secondary_nav').removeClass('sticky').addClass('not_sticky');  
	         $('#main').css("padding-top", 0);     
	     }
	});
	
	$('.video').click(function (e) {
        e.preventDefault(); // disable normal link function so that it doesn't refresh the page
        var id = $(this).attr('id');
		$('.modal').addClass("open");
		$( '.modal .embed_container' ).html( "<iframe src='//player.vimeo.com/video/" + id + "?color=ff6c38&amp;autoplay=1' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>" ).show();
    });
    
    $('.close_link').click(function(){
    	$('.modal').removeClass("open"); // hide the overlay
    	$('.modal .embed_container').empty().hide();
    });
    
    $('.modal').click(function(){
    	$('.modal').removeClass("open");
    	$('.modal .embed_container').empty().hide();
    });
	
});
