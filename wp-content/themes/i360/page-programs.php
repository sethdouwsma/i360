<?php 
/* 
Template Name: How We Work
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>

	<div class="secondary_nav not_sticky">
		<div class="container clearfix">
			<ul class="navigation">
				<li><a href="#services">Services</a></li>
				<li><a href="#life_development">Life Development®</a></li>
				<li><a href="#mental_health">Mental Health</a></li>
				<li><a href="#addiction">Addiction</a></li>
				<li><a href="#emerging_adulthood">Emerging Adulthood</a></li>
			</ul>
		</div>
	</div>
	
	<div id="services" class="section">
		<div class="container borderbottom">
			<div class="intro">
				<h2>THE <span>i<span>360</span></span> Services</h2>
				<hr>
				<?php the_field('programs_overview'); ?>
			</div>
		</div>
		
		<?php if(have_rows('programs')) : ?>
			<div class="programs_wrapper">
				<hr>
				<ul class="tab_nav program_images">
				<?php $count = 1; ?>
				<?php while(have_rows('programs')) : the_row(); ?>
					<?php $image = get_sub_field('image'); $size = 'program-thumb'; ?>
					<li><a href="#" name="<?php echo '#'.$count++ ?>"><?php echo wp_get_attachment_image( $image, $size ); ?></a></li>
				<?php endwhile; ?>
				</ul>
				<hr>
				<div class="container tab_wrapper program_description_wrapper">
				<?php $count = 1; ?>
				<?php while(have_rows('programs')) : the_row(); ?>
					<?php $image = get_field('image'); $size = 'program-thumb'; ?>
					<div id="<?php echo '#'.$count++ ?>" class="program_description">
						<h2 class="line" style="text-transform: none;"><?php the_sub_field('name'); ?></h2>
						<?php the_sub_field('description'); ?>
					</div>
				<?php endwhile; ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
	
	<div id="life_development">
		<div class="life_development_intro section">
			<div class="pattern"></div>
			<div class="intro">
				<h2>Life Development</h2>
				<hr>
				<?php the_field('life_development_overview'); ?>
			</div>
		</div>
		<div class="container section life_development_bottom">
			<div class="what_is_life_development">
				<h2 class="line">What Is Life Development</h2>
				<?php the_field('what_is_life_development'); ?>
				<p class="button"><a href="<?php echo get_permalink(17); ?>" class="button-gray-solid">Let's Connect</a></p>
			</div>
			<div class="life_development_video">
				<div class="embed_container"><?php the_field('life_development_video'); ?></div>
			</div>
		</div>
	</div>
	
	<div id="who_we_work_with">
		<div class="pattern"></div>
		<div class="container">
			<h1>WHO i360 WORKS WITH</h1>
			<p><?php the_field('who_i360_works_with'); ?></p>
		</div>
	</div>
	
	<div id="mental_health">
		<div class="container intro section">
			<h2>Mental Health</h2>
			<hr>
			<?php the_field('mental_health_overview'); ?>
		</div>
		<div class="mental_health_bottom row clearfix">
			<div class="mental_health_specifics section container">
				<div class="specific">
					<h2 class="line">Anxiety</h2>
					<?php the_field('anxiety'); ?>
				</div>
				<div class="specific">
					<h2 class="line">Depression</h2>
					<?php the_field('depression'); ?>
				</div>
				<div class="specific">
					<h2 class="line">Bipolar</h2>
					<?php the_field('bipolar'); ?>
				</div>
			</div>
			<div class="mental_health_stats section">
				<div class="pattern"></div>
				<div class="wrapper">
				<h2 class="line">Statistics</h2>
				<?php if(have_rows('mental_health_stats')) : ?>
					<ul class="stats">
					<?php while(have_rows('mental_health_stats')) : the_row(); ?>
						<li>
							<h1 class="stat"><?php the_sub_field('number'); ?></h1>
							<?php the_sub_field('description'); ?>
						</li>
					<?php endwhile; ?>
					</ul>
				<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	
	<div id="addiction">
		<div class="intro section container">
			<h2>Addiction</h2>
			<hr>
			<?php the_field('chemical_dependency_overview'); ?>
			<p class="button"><a href="http://rethinkingdrinking.niaaa.nih.gov/IsYourDrinkingPatternRisky/WhatsYourPattern.asp" class="button-gray-solid" target="_blank">Am I An Addict?</a></p>
		</div>
		<ul class="chemical_dependency_images">
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/cd_image_1.jpg" alt="Chemical Dependency"></li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/cd_image_2.jpg" alt="Chemical Dependency"></li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/cd_image_3.jpg" alt="Chemical Dependency"></li>
		</ul>
	</div>	
	
	<div id="emerging_adulthood" class="section container borderbottom">
		<div class="intro">
			<h2>Emerging Adulthood</h2>
			<hr>
			<?php the_field('emerging_adulthood_overview'); ?>
			<p class="button"><a href="<?php echo get_permalink(17); ?>" class="button-gray-solid">Lets Work Together</a></p>
		</div>
	</div>	
	
<?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>
