<?php 
/* 
Template Name: Home
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	
	<div class="container section">
		<div class="intro">
			<h2>WHY <span>i<span>360</span></span>?</h2>
			<hr>
			<?php the_field('why_i360'); ?>
			<p class="button"><a href="<?php echo get_permalink(17); ?>" class="button-gray-solid">Join Us</a></p>
		</div>
		
		<div class="half_logo_wrapper">
			<div class="half_logo"><img src="<?php echo get_template_directory_uri(); ?>/images/half_logo.png"></div>
		</div>
		
		<?php if( have_rows('home_page_programs') ): ?>
		<ul class="hp_services">
			<?php while( have_rows('home_page_programs') ): the_row(); ?>
				<li class="hp_service">
					<h2 class="line"><?php the_sub_field('title'); ?></h2>
					<?php the_sub_field('description'); ?>
					<p class="button"><a href="<?php the_sub_field('link'); ?>" class="button-gray-solid">Learn More</a></p>
				</li>
			<?php endwhile; ?> 
		</ul>
		<?php endif; ?>
	</div>
	
	<div class="testimonials section" data-stellar-ratio="2">
		<div class="pattern"></div>
		<?php if( have_rows('testimonials') ): ?>
		<div class="testimonial_slider slider">
			<ul class="slider__wrapper">
				<?php while( have_rows('testimonials') ): the_row(); ?>
					<li class="slider__item">
						<div class="intro">
							<h2><?php the_sub_field('name'); ?></h2>
							<hr>
							<?php the_sub_field('testimony'); ?>
						</div>
					</li>
				<?php endwhile; ?> 
			</ul>
		</div>
		<?php endif; ?>
	</div>
	<div class="hp_locations section">
		<div class="container intro">
			<h2>Locations</h2>
			<hr>
			<?php the_field('locations'); ?>
			<p class="button"><a href="<?php echo get_permalink(11);?>" class="button-gray-solid">Meet Our Team</a></p>
		</div>
	</div>
	
	<div class="hp_news container">
		<div class="section_header">
			<h2 class="line">News</h2>
			<p class="button"><a href="<?php echo get_permalink(13);?>" class="button-gray-solid">All Articles</a></p>
		</div>
		<?php
		 	$args = array(
	            'posts_per_page' => 3,
			    'orderby'	=> 'date',
				'order'		=> 'DESC' //  Newst To Oldest
	        );
			query_posts( $args ); 
		?>
	  	<?php if ( have_posts() ) : ?>
		  	<ul class="news_buckets horizontal">
				<?php while ( have_posts() ) : the_post(); ?>
					<li class="news_bucket">
						<?php get_template_part( 'content', 'bucket' ); ?>
					</li>
				<?php endwhile; ?>
			</ul>
	  	<?php endif; ?>
		<?php wp_reset_query(); ?>
	</div>	
	
<?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>
