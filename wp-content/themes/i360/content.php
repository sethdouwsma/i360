<?php
/**
 * The default template for displaying post content
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry_thumb">
		<div class="entry_date"><span><?php the_time('m/d'); ?></span></div>
		<?php if(get_the_post_thumbnail()) : ?>
			<p><?php the_post_thumbnail('vertical-bucket'); ?></p>
		<?php else : ?>
			<p><?php echo wp_get_attachment_image( 21, 'vertical-bucket' ); ?></p>
		<?php endif; ?>
	</div>
	<div class="entry_content">
		<h2 class="entry_title"><?php the_title(); ?></h2>
		<p class="entry_meta">Posted in <?php the_category(', ') ?></p>
		<?php the_content(); ?>
	</div>
</article>