<?php get_header(); ?>

	<div class="categories_wrapper small_container aligncenter">
		<h2>Categories</h2>
		<hr>
		<ul class="categories clearfix">
			<?php wp_list_categories('title_li=&orderby=name'); ?>
		</ul>
	</div>

	<div class="content section container small_container">
	  	<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>
	  	<?php else : ?>
	  		<h1>There are currently no posts</h1>
	  	<?php endif; ?>
	  	<?php wp_reset_query(); ?>
		<div class="post_navigation clearfix">
			<div class="post_nav_item previous"><?php previous_post_link('%link'); ?></div>
			<div class="post_nav_item next"><?php next_post_link('%link'); ?></div>
		</div><!-- end post_navigation -->
	
		<div class="comments">
			<?php comments_template(); ?> 
		</div>
	</div><!-- end content -->

<?php get_footer(); ?>