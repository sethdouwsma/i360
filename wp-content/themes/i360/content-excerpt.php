<?php
/**
 * The default template for displaying post content in vertical buckets
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry_thumb">
		<div class="entry_date"><span><?php the_time('m/d'); ?></span></div>
		<?php if(get_the_post_thumbnail()) : ?>
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('vertical-bucket'); ?></a>
		<?php else : ?>
			<a href="<?php the_permalink(); ?>"><?php echo wp_get_attachment_image( 21, 'vertical-bucket' ); ?></a>
		<?php endif; ?>
	</div>
	<div class="entry_content">
		<h2 class="entry_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<p class="entry_meta">Posted in <?php the_category(', ') ?></p>
		<?php
		  $excerpt = get_the_excerpt();
		  echo string_limit_words($excerpt,70);
		?>
		<p class="read_more"><a href="<?php the_permalink(); ?>" class="button-gray-solid">Read Full Article</a></p>
	</div>
</article>