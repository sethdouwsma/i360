<?php 
/* 
Template Name: Contact
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	
	<div class="section container contact_wrapper">
		<div class="office_locations">
			<h2 class="line">Office Locations</h2>
			<ul class="contact_locations">
			<?php
				$children = get_pages( 
			    array(
			        'sort_column' => 'menu_order',
			        'sort_order' => 'ASC',
			        'hierarchical' => 0,
			        'parent' => 11,
			    ));
			 ?>
			 <?php foreach( $children as $post ) : setup_postdata( $post ); ?>
			 	<li>
					<h4><?php the_title(); ?></h4>
					<p><strong>Phone:</strong> <?php the_field('phone'); ?></p>
					<p><strong>Fax:</strong> <?php the_field('fax'); ?></p>
					<p><strong>Address:</strong> <?php the_field('address'); ?></p>
				</li>
			<?php endforeach; ?>
			</ul>
		</div>
		<div class="form_wrapper">
			<?php if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 2 ); } ?>
		</div>
	</div>
	
<?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>
